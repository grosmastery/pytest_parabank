import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
import time


@pytest.fixture
def driver():
    hub_url = "http://localhost:4444/wd/hub"
    desired_cap = DesiredCapabilities.CHROME

    driver = webdriver.Remote(
        command_executor=hub_url,
        desired_capabilities=desired_cap
        )

    driver.implicitly_wait(10)
    driver.maximize_window()
    yield driver 
    driver.quit()

@pytest.fixture(autouse=True)
def delete_user(driver):
    driver.get("https://parabank.parasoft.com/parabank/admin.htm")
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.NAME, "action"))).click()

@pytest.fixture(autouse=True)
def sign_up(driver):
    driver.get("https://parabank.parasoft.com/parabank/register.htm")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.firstName"))).send_keys("testName")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.lastName"))).send_keys("testLastName")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.address.street"))).send_keys("testAddress")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.address.city"))).send_keys("testCity")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.address.state"))).send_keys("testState")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.address.zipCode"))).send_keys(1000)
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.ssn"))).send_keys(100)
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.username"))).send_keys("test")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "customer.password"))).send_keys("test")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "repeatedPassword"))).send_keys("test")
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="customerForm"]/table/tbody/tr[13]/td[2]/input'))).click()

def logout(driver):
    driver.get("https://parabank.parasoft.com/parabank/index.htm")
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="leftPanel"]/ul/li[8]/a'))).click()

def transfer_funds(driver):
    driver.get("https://parabank.parasoft.com/parabank/transfer.htm")

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "amount"))).send_keys(Keys.F5)
    time.sleep(0.2)
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "amount"))).send_keys(1)
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "amount"))).send_keys(Keys.ENTER)

def test_title(driver):

    driver.get("https://parabank.parasoft.com/parabank/index.htm")

    assert driver.title == "ParaBank | Welcome | Online Banking"

def test_sign_up(driver):

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="leftPanel"]/p'))).text == "Welcome testName testLastName"

def test_logout(driver):

    logout(driver)

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="leftPanel"]/h2'))).text == "Customer Login"

def test_login(driver):

    logout(driver)

    driver.get("https://parabank.parasoft.com/parabank/index.htm")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.NAME, "username"))).send_keys("test")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.NAME, "password"))).send_keys("test")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.NAME, "password"))).send_keys(Keys.ENTER)

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="leftPanel"]/p'))).text == "Welcome testName testLastName"

def test_customer_care(driver):
    driver.get("https://parabank.parasoft.com/parabank/contact.htm")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "name"))).send_keys("testName")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "email"))).send_keys("tesmEmail@test.com")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "phone"))).send_keys(12345667890)
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "message"))).send_keys("testMessage")
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="contactForm"]/table/tbody/tr[5]/td[2]/input'))).click()

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="rightPanel"]/p[1]'))).text == "Thank you testName"

def test_open_new_account(driver):

    driver.get("https://parabank.parasoft.com/parabank/openaccount.htm")
    time.sleep(0.2)
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '#rightPanel > div > div > form > div > input'))).click()

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="rightPanel"]/div/div/p[1]'))).text == "Congratulations, your account is now open."


def test_transfer_funds(driver):

    transfer_funds(driver)

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="rightPanel"]/div/div/p[2]'))).text == 'See Account Activity for more details.'

def test_bill_pay(driver):
    
    driver.get("https://parabank.parasoft.com/parabank/billpay.htm")

    account_id = driver.find_element(By.NAME, "fromAccountId").text.split()[0]

    driver.find_element(By.NAME, "payee.name").send_keys("testName")
    driver.find_element(By.NAME, "payee.address.street").send_keys("testAddress")
    driver.find_element(By.NAME, "payee.address.city").send_keys("testCity")
    driver.find_element(By.NAME, "payee.address.state").send_keys("testState")
    driver.find_element(By.NAME, "payee.address.zipCode").send_keys(1)
    driver.find_element(By.NAME, "payee.phoneNumber").send_keys(1234567890)
    driver.find_element(By.NAME, "payee.accountNumber").send_keys(account_id)
    driver.find_element(By.NAME, "verifyAccount").send_keys(account_id)
    driver.find_element(By.NAME, "amount").send_keys(1)
    driver.find_element(By.XPATH, '//*[@id="rightPanel"]/div/div[1]/form/table/tbody/tr[14]/td[2]/input ').click()

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="rightPanel"]/div/div[2]/h1'))).text == 'Bill Payment Complete'

def test_update_contact_info(driver):

    driver.get('https://parabank.parasoft.com/parabank/updateprofile.htm')
    time.sleep(0.2)
    driver.find_element(By.NAME, "customer.firstName").send_keys("1")
    driver.find_element(By.XPATH, '//*[@id="rightPanel"]/div/div/form/table/tbody/tr[8]/td[2]/input').click()

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="rightPanel"]/div/div/p'))).text == 'Your updated address and phone number have been added to the system.'

def test_find_transaction_by_id(driver):

    driver.get("https://parabank.parasoft.com/parabank/transfer.htm")
    time.sleep(0.2)
    account_id = driver.find_element(By.ID, "fromAccountId").text.split()[0]

    transfer_funds(driver)

    driver.get(f"https://parabank.parasoft.com/parabank/activity.htm?id={account_id}")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="transactionTable"]/tbody/tr[1]/td[2]/a'))).click()
    transaction_id = driver.find_element(By.XPATH, '//*[@id="rightPanel"]/table/tbody/tr[1]/td[2]').text.split()[0]

    driver.get("https://parabank.parasoft.com/parabank/findtrans.htm")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "criteria.transactionId"))).send_keys(transaction_id)
    driver.find_element(By.ID, "criteria.transactionId").send_keys(Keys.ENTER)

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="transactionTable"]/tbody/tr/td[2]/a'))).text == "Funds Transfer Sent"

def test_find_transaction_by_date(driver):

    date = datetime.now().strftime('%m-%d-%Y')
    
    transfer_funds(driver)

    driver.get("https://parabank.parasoft.com/parabank/findtrans.htm")
    driver.find_element(By.ID, "criteria.onDate").send_keys(date)
    driver.find_element(By.XPATH, '//*[@id="rightPanel"]/div/div/form/div[5]/button').click()
    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="transactionTable"]/tbody/tr/td[2]/a'))).text == "Funds Transfer Sent"

def test_find_transaction_by_date_range(driver):

    date = datetime.now().strftime('%m-%d-%Y')

    transfer_funds(driver)

    driver.get("https://parabank.parasoft.com/parabank/findtrans.htm")
    driver.find_element(By.ID, "criteria.fromDate").send_keys(date)
    driver.find_element(By.ID, "criteria.toDate").send_keys(date)
    driver.find_element(By.XPATH, '//*[@id="rightPanel"]/div/div/form/div[7]/button').click()

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="transactionTable"]/tbody/tr/td[2]/a'))).text == "Funds Transfer Sent"

def test_find_transaction_by_amount(driver):
    
    transfer_funds(driver)

    driver.get("https://parabank.parasoft.com/parabank/findtrans.htm")
    driver.find_element(By.ID, "criteria.amount").send_keys(1)
    driver.find_element(By.XPATH, '//*[@id="rightPanel"]/div/div/form/div[9]/button').click()

    assert WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="transactionTable"]/tbody/tr/td[2]/a'))).text == "Funds Transfer Sent"
