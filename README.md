## **Automating Web Testing with Python and Selenium**
This repository contains a Python script for automating functional testing of a web application using the Selenium library.

### **Getting Started**
Prerequisites To use this script, you need the following installed:

- Python 3.x
- Pytest
- Selenium

### **Running the tests**
To run the tests, execute the following command:
```
pytest
```

### **Overview**
This script automates the testing of a web application using the Selenium library. It tests the following functionality:

- Signing up a new user
- Logging in as a user
- Logging out
- Contacting customer care
- Transferring funds

### **Test Cases**
The script contains the following test cases:

`Test Title`

Verifies that the title of the home page is correct.

`Test Sign Up`

Verifies that a new user can sign up successfully.

`Test Login`

Verifies that a user can log in successfully.

`Test Logout`

Verifies that a user can log out successfully.

`Test Customer Care`

Verifies that a user can contact customer care successfully.

`Test Transfer Funds`

Verifies that a user can transfer funds successfully.

`Fixtures`

The script also contains the following fixtures:

`Driver`

A fixture that sets up and tears down the Selenium webdriver.

`Delete User`

A fixture that deletes a user from the system after each test.

`Sign Up`

A fixture that signs up a new user before each test.

### **Conclusion**
This script provides a basic framework for automating functional testing of a web application using Python and Selenium.
